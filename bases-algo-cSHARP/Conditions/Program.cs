﻿using System;

namespace Conditions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // If 
            if (2 == 1 + 1)
                Console.WriteLine("2 = 2");

            string a = "bonjour";
            string b = "au revoir";

            // If else
            if(b == "bonjour")
            {
                Console.WriteLine("a = "+ a);
            }
            else
            {
                Console.WriteLine("b = " + b);
            }
            
            // If imbriquee
            if(a == "bonjour")
            {
                if(b == "au revoir")
                {
                    Console.WriteLine("vous avez dit bonjour et aurevoir");
                }
            }
            int i = 0;
            i++;
            i = i + 1;
            ++i;
            i = 1 + i;
            

            //CRI DE L'ANIMAL
            Console.WriteLine("Veuillez selection un animal ?");
            Console.WriteLine("1 - chiens");
            Console.WriteLine("2 - chat");
            string read = Console.ReadLine();

            if (! (true))
            {
                
                    Console.WriteLine("vous avez dit bonjour et aurevoir");
            
            }

            if (read == "1" || read == "2" )
            {
                if(read == "1")
                {
                    Console.WriteLine("WAF WAF !!");
                }
                else
                {
                    Console.WriteLine("MIAOU");
                }
            }


            //Note éléves
            Console.Write("Quel note a eu l'eleve ? ");
            int note = Convert.ToInt32(Console.ReadLine());
            if(note > 0 && note <21)
            {
                switch (note)
                {
                    case 10 :
                        Console.WriteLine("Passable");
                        Console.WriteLine("Mais pas fait d'effort");
                        break;
                    case 11:
                        Console.WriteLine("Passable");
                        break;
                    case 12:
                        Console.WriteLine("Assez bien");
                        break;
                    case 13:
                        Console.WriteLine("Assez bien");
                        break;
                    case 14:
                        Console.WriteLine("Bien");
                        break;
                    case 15:
                        Console.WriteLine("Bien");
                        break;
                    case 16:
                        Console.WriteLine("Trés bien");
                        break;
                    case 17:
                        Console.WriteLine("Trés bien");
                        break;
                    case 18:
                        Console.WriteLine("Trés bien");
                        break;
                    case 19:
                        Console.WriteLine("Trés bien");
                        break;
                    case 20:
                        Console.WriteLine("Trés bien");
                        break;
                    default:
                        Console.WriteLine("NON admis");
                        break;
                }
            }
            
        }
    }
}
