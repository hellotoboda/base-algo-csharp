﻿using System;
using System.Collections.Generic;

namespace Boucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Tableaux
            int[] a = { 1, 2, 3, 4, 5 };
            string[] b = { "bonjour", "hello", "buongiorno" };
            
            //Listes
            List<int> listeInt = new List<int>();
            listeInt.Add(10);
            listeInt.Add(20);
            listeInt.Add(30);
            listeInt.Add(40);
            listeInt.Add(50);
            listeInt.Add(60);
             
            
            for(int i =0; i < a.Length; i++)
            {
                Console.WriteLine("FOR " + a[i].ToString());
            }

            int compteur = 0;
            while (compteur < a.Length)
            {
                Console.WriteLine("WHILE : " + compteur.ToString());
                compteur++;
            }
            
            foreach (int nombre in a)
            {
                Console.WriteLine("FOREACH " + nombre.ToString());
            }
            

            //Dictionnaire
            Dictionary<int, string> dic = new Dictionary<int, string>();
            dic.Add(1, "ensemble d'instruction résolvant un probléme");
            dic.Add(2, "etape 2");
            dic.Add(3, "etape 3");
            dic.Add(4, "etape 4");
            dic.Add(4, "2");

            // 
            foreach (var item in dic)
            {
                Console.WriteLine("Key = {0}, Value = {1}", item.Key, item.Value);
                Console.WriteLine("Key = "+ item.Key+ ", Value = " + item.Value);
            }
            
            /*
            for(int tartanpion = 0; tartanpion < 10; tartanpion++)
            {
                Console.WriteLine(tartanpion+1);
            }

            for (int tartanpion = 1; tartanpion <= 10; tartanpion++)
            {
                Console.WriteLine(tartanpion);
            }
            */

            
            // Fibonacci 
            Console.WriteLine("Veuillez entrer le paramétre d'arret");
            int param = Convert.ToInt32(Console.ReadLine());
            int fibo = 0;
            int facto = 1;
            for (int i = 1; i <= param; i++)
            {
                fibo = fibo + i;
            }

            for (int i = 1; i <= param; i++)
            {
                facto = facto * i;
            }
            Console.WriteLine("la factorielle est : {0} et la sommes de la suite de fibonnacci est : {1}", facto.ToString(), fibo.ToString()) ;
            



            //Palyndrome
            Console.WriteLine("Veuillez spécifier votre mot à vérifier ");
            string palyndrome = Console.ReadLine();
            string mot_inverse = "";
            //lion => noil
            for(int i = palyndrome.Length-1 ; i >= 0 ; i--)
            {
                mot_inverse = mot_inverse + palyndrome.Substring(i,1);
            }

            Console.WriteLine(palyndrome);
            Console.WriteLine(mot_inverse);

            if(mot_inverse == palyndrome)
            {
                Console.WriteLine("C'est un palindrome");
            }
            else
            {
                Console.WriteLine("C'est pas un palindrome");
            }
        }
    }
}
