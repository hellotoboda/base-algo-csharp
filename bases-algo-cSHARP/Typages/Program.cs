﻿using System;
using System.Collections.Generic;

namespace Typages
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*
            int entier = 1000;
            float nombre_flottante = 1.2f;
            double double_entier = 1.3;

            char charactere = 'a';
            string chaine_characteres = "bonjour";

            bool bouleen = true;
            */

            // Le typage 
            int conversion = Convert.ToInt32("1");
            int test = conversion + 5;
            Console.WriteLine(test);
            /*
            bool result;
            string str_var = "111";
            int num_var;
            result = Int32.TryParse(str_var, out num_var);
            if (result)
                Console.WriteLine(num_var);
            else
                Console.WriteLine("Invalid String");

            // Affichage
            Console.WriteLine(entier);
            Console.WriteLine(nombre_flottante);
            Console.WriteLine(double_entier);
            Console.WriteLine(charactere);
            Console.WriteLine(chaine_characteres);
            Console.WriteLine(bouleen);
            */



            int a = 12;
            int b = 13;
            int garder = 0;
            if(a > b)
            {
                garder = a;
            }
            else
            {
                garder = b;
            }
            Console.WriteLine(garder);
        }
    }
}
