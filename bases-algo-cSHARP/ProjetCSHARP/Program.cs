﻿using System;


namespace ProjetCSHARP
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("Entrez un chiffre ou un nombre");
            string nb1 = Console.ReadLine();
            Console.WriteLine("Entrez un autre nchiffre ou un nombre");
            string nb2 = Console.ReadLine();
            //Instanciation + init variable
            int nb1Int = 0;
            int nb2Int = 0;
            //Conversion des données récupéré
            Console.WriteLine("Convertision des variables en cours");
            nb1Int = Convert.ToInt32(nb1);
            nb2Int = Convert.ToInt32(nb2);
            //Le calcul de l'addition 
            int resultat = nb1Int + nb2Int;
            
            Console.WriteLine("Les résultat est " + resultat);
            Console.ReadLine();
            */
            /*
            string text1, text2, text3 = "";

            //Récupération des texts
            Console.WriteLine("Entrez du text");
            text1 = Console.ReadLine();
            Console.WriteLine("Entrez du text");
            text2 = Console.ReadLine();
            Console.WriteLine("Entrez du text");
            text3 = Console.ReadLine();

            string resultat = text1 + " " + text2 + " " + text3;
            Console.WriteLine(resultat);
            //Temporisation 
            Console.ReadLine();
            */
            /*
            //Récupération au clavier
            string recup = Console.ReadLine();
            //Affichage de la récupération
            Console.WriteLine(recup);
            */


            //Division normal
            //5 / 2 = 2.5
            //Division Euclidienne
            //Signe du modulo:
            /*
            int modulo = 5 % 2;
            Console.WriteLine(modulo);
            Console.ReadLine();
            */
            //Temporisation
            //Console.ReadLine();


            /*
            //Variables
            int num1 = 1;
            int num2 = 2;

            Console.WriteLine(num1.ToString());

            float num3 = 2.1f;
            double num4 = 3.5f;

            char num5 = 'a';
            string num6 = "Hello World!";
            string num7 = " C'est un test";

            char[] tabChar = { 'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', '!' };
            string[] tabString = { "Hello", "World", "!"};
            int[] tabInt = { 1, 2, 3, 4, 5, 6 };

            // Utilisation Condition
            #region CONVERSION
            Double d2 = Double.Parse(num3.ToString()); 
            float f2 = float.Parse(num4.ToString());
            float result = num3 + f2;
            #endregion
            #region CALCULE
            int num = 0;
            num = num1 + num2;
            float res2 = num + num3;
            //float res3 = num3 + num4;
            string res3 = num6 + num7;
            #endregion
            */
            /*
            #region CONDITION
            if (num5 == 'a')
            {
                Console.WriteLine("La condition a renvoyé vrai");
            }

            if (num5 == 'b')
            {
                Console.WriteLine("La condition a renvoyé vrai");
            }
            else
            {
                Console.WriteLine("La réponse est false");
            }

            if (num5 == 'b')
                Console.WriteLine("La condition a renvoyé vrai");
            else
                Console.WriteLine("La réponse est false");

            if (num5 == 'b')
            {
                Console.WriteLine("La condition a renvoyé vrai");
            }
            else if(num5 == 'a')
            {
                Console.WriteLine("La réponse est false puis vrai");
            }

            switch(num5)
            {
                case 'a':
                    Console.WriteLine("La réponse est a");
                    break;
                case 'b':
                    Console.WriteLine("La réponse est b");
                    break;
                default:
                    Console.WriteLine("Défaut");
                    break;
            }
            #endregion
            */

            /*
            #region BOUCLE
            int i = 0;
            while(i == tabInt.Length-1)
            {
                Console.WriteLine(tabInt[i]);
                i++;
            }

            for(int j=0; j < tabChar.Length-1; j++)
            {
                Console.WriteLine(tabChar[j]);
            }

            foreach(string c in tabString)
            {
                Console.WriteLine(c);
            }
            #endregion
            */
            /*
            Console.WriteLine("Entrez une valeur pour finir le programme");
            Console.ReadLine();
            */
            /*
            #region Condition
            char a = 'a';
            if (a == 'b')
            {
                Console.WriteLine("La condition a renvoyé vrai");
            }
            else if (a == 'a')
            {
                Console.WriteLine("La réponse est false puis vrai");
            }

            switch (a)
            {
                case 'a':
                    Console.WriteLine("La réponse est a");
                    break;
                case 'b':
                    Console.WriteLine("La réponse est b");
                    break;
                default:
                    Console.WriteLine("Défaut");
                    break;
            }
            #endregion

            #region List, Tableau, dictionnaire
            //Tableau de longueur : 6
            //premiére position : 0
            //derniere position : 5
            //Utilisation de boucles : while (do while), for, foreach
            int[] tabInt = { 1, 2, 3, 4, 5, 1000, 10 };
            string[] tabString = {  };
            
            List<string> listCourse = new List<string>();
            listCourse.Add("100g de créme fraiche");
            listCourse.Add("2 oignons");

            Dictionary<int, string> tabDictionary = new Dictionary<int, string>();
            Dictionary<float, Boolean> tabDictionary2 = new Dictionary<float, Boolean>();
            tabDictionary.Add(1, "string");
            tabDictionary.Add(2, "int");
            #endregion
            */

            //Créer une variable de type string
            // char a = '' => variable de type char (charactére) initialisé a vide;
            string prenom = "";
            //Demander une saisie 
            Console.WriteLine("Entrez du text");
            //Faire une saisie 
            prenom = Console.ReadLine();
            //Créer une constante avec votre prénom
            const string PRENOM_THIBAUT = "Thibaut";
            //Faire une condition pour vérifier les valeur (entre constante et la saisie)
            //Si c'est vérifier, on dira que c'est bien ton prénom et si c'est pas vérifier on dira que c'est pas ton prénom.
            if(prenom == PRENOM_THIBAUT)
            {
                Console.WriteLine("OK");
            }
            else
            {
                Console.WriteLine("NON");
            }

            Console.ReadLine();

        }
    }
}
