﻿using System;

namespace LesFonctions
{
    internal class Program
    {
         static void Main(string[] args)
         {
            int a = 0;
            a = Addition(1, 2);
            AfficherHelloWorld();
            AfficherResultat(a);
            AfficherResultat(Soustraction(1,2));
            AfficherResultat(Soustraction(Addition(1,2), Addition(1, 2)));
            Afficher("auto save file actived");
            Afficher("Voila pour les fonctions");
         }

        public static int Addition(int a, int b)
        {
            return a + b;
        }
        // je suis un commentaire 
        /* 
         * 
         * Je suis un commentaire 
         */
        public static int Soustraction(int a, int b)
        {
            return a - b;
        }

        public static  void AfficherHelloWorld()
        {
            Console.WriteLine("HELLO WORLD !!!!!!");
        }

        public static void AfficherResultat(int resultat)
        {
            Console.WriteLine($"Le résultat est : {resultat.ToString()}");
        }

        public static string Afficher(string aff)
        {
            Console.WriteLine($"Le résultat est : " + aff);
            return aff;
        }
    }
}
