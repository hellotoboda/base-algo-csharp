﻿using System;

namespace globallisateur
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a = 1;
            int b = 2;
            int c = 3;

            if(a == 0)
            {
                Console.WriteLine("a = 0");
            }else
            {
                if (a == 1)
                {
                    Console.WriteLine("a = 1");
                }
                else
                {
                    if(a == 2)
                    {
                        Console.WriteLine("a = 2");
                    }
                }
            }

            switch(a)
            {
                case 0:
                    Console.WriteLine("a = 0");
                    break;
                case 1:
                    Console.WriteLine("a = 1");
                    break;
                case 2:
                    Console.WriteLine("a = 2");
                    break;
                default: 
                    Console.WriteLine("a = 4");
                    break;
            }

        }
    }
}
