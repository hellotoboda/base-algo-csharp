﻿using System;
using ConsoleApp1.Metier;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {

            ConsoleApp1.Metier.Class1 classe = new ConsoleApp1.Metier.Class1();
            classe.a = 4;
            classe.b = 10;
            classe.c = 20;

            Class1 class2 = new Class1(7, 10000, 154);
            Console.WriteLine(classe.a.ToString() + classe.b.ToString() + classe.c.ToString());
            Console.WriteLine(class2.a.ToString() + class2.b.ToString() + class2.c.ToString());
            

            const double IMPOSITION = 0.3;
            const double IMPOSITION2 = 0.3;
            double annee1 = 12000;
            double annee2 = 24000;


            double impositionAnnee1 = annee1 * IMPOSITION;
            double impositionAnnee2 = annee2 * IMPOSITION;

            Console.WriteLine(impositionAnnee1.ToString() + " " + impositionAnnee2.ToString());
        }
    }
}
